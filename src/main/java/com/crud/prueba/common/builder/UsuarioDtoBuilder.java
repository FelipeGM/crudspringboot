package com.crud.prueba.common.builder;

import com.crud.prueba.common.model.DTOs.UsuarioDto;

public class UsuarioDtoBuilder {

    private String nombre;
    private String apellido;
    private String profesion;
    private String email;

    public UsuarioDtoBuilder() {
    }

    public UsuarioDtoBuilder setNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public UsuarioDtoBuilder setApellido(String apellido) {
        this.apellido = apellido;
        return this;
    }

    public UsuarioDtoBuilder setProfesion(String profesion) {
        this.profesion = profesion;
        return this;
    }

    public UsuarioDtoBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public UsuarioDto build(){
        return new UsuarioDto(nombre,apellido,profesion,email);
    }
}
