# README #

Aplicacion hecha con spring boot utilizando spring-boot-web para la exposicion de los servicios y spring-boot-jpa como ORM

### How do I get set up? ###

Ejecutar los siguientes comandos desde la carpeta del proyecto

* Ejecutar el comando "mvn clean"
* Ejecutar el comando "mvn spring-boot:run"

Luego de ejecutar los comandos el servidor debe quedar corriendo en el puerto 8081.


	Nota: cada vez que se suben los servicios la base de datos es truncada ya que se agreg� la propiedad
	"spring.jpa.hibernate.ddl-auto=create"

### APIs ###

* http://localhost:8081/crud-app/v1/usuarios (Servicio post que crea los usuarios)
	
	*Request de ejemplo
	`{	
	"nombre":"Felipe",
	"apellido":"Garcia",
	"profesion":"Ing de sistemas",
	"email":"felipe@gmail.com"
	}`
	
	
* http://localhost:8081/crud-app/v1/usuarios (Servicio get que devuelve todos los usuarios en db) 
* http://localhost:8081/crud-app/v1/usuarios/{id} (Servicio get que obtiene un usuario dado un id)

	En la carpeta Coleccion postman se encuentra los servicios.
