package com.crud.prueba.web.api.v1.usuario;

import com.crud.prueba.common.model.DTOs.UsuarioDto;
import com.crud.prueba.service.usuario.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/crud-app/v1")
public class UsuarioApiImpl implements IUsuarioApi{

    @Autowired
    private IUsuarioService iUsuarioService;

    @Override
    @PostMapping("/usuarios")
    public ResponseEntity create(@RequestBody UsuarioDto usuarioDto) {
        return iUsuarioService.create(usuarioDto);
    }

    @Override
    @GetMapping("/usuarios")
    public ResponseEntity findAll() {
        return iUsuarioService.findAll();
    }

    @Override
    @GetMapping("/usuarios/{id}")
    public ResponseEntity findById(@PathVariable("id") int id) {
        return iUsuarioService.findById(id);
    }
}
