package com.crud.prueba.common.converter;

import com.crud.prueba.common.builder.UsuarioBuilder;
import com.crud.prueba.common.builder.UsuarioDtoBuilder;
import com.crud.prueba.common.model.DTOs.UsuarioDto;
import com.crud.prueba.common.model.entity.Usuario;
import org.springframework.stereotype.Component;

@Component
public class UsuarioConverter {

    public Usuario usuarioDtoToUsuario(UsuarioDto usuarioDto){
        Usuario usuario = new UsuarioBuilder()
                .setNombre(usuarioDto.getNombre())
                .setApellido(usuarioDto.getApellido())
                .setEmail(usuarioDto.getEmail())
                .setProfesion(usuarioDto.getProfesion())
                .build();
        return  usuario;
    }

    public UsuarioDto UsuarioToUsuarioDto(Usuario usuario){
        UsuarioDto usuarioDto = new UsuarioDtoBuilder()
                .setNombre(usuario.getNombre())
                .setApellido(usuario.getApellido())
                .setProfesion(usuario.getProfesion())
                .setEmail(usuario.getEmail())
                .build();

        return  usuarioDto;
    }
}
