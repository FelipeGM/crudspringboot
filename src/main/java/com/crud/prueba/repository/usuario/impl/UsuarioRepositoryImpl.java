package com.crud.prueba.repository.usuario.impl;

import com.crud.prueba.common.model.entity.Usuario;
import com.crud.prueba.repository.usuario.IUsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UsuarioRepositoryImpl implements IUsuarioRepositoryFacade {

    @Autowired
    private IUsuarioRepository iUsuarioRepository;

    @Override
    public Optional<Usuario> create(Usuario usuario) {
        return Optional.of(iUsuarioRepository.save(usuario)) ;
    }

    @Override
    public List<Usuario> getUsuarios() {
        return iUsuarioRepository.findAll();
    }

    @Override
    public Optional<Usuario> getUsuarioById(int id) {
        return iUsuarioRepository.findById(id);
    }
}
