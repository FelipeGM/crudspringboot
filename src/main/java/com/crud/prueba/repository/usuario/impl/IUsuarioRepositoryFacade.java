package com.crud.prueba.repository.usuario.impl;

import com.crud.prueba.common.model.entity.Usuario;

import java.util.List;
import java.util.Optional;

public interface IUsuarioRepositoryFacade {

    Optional<Usuario> create(Usuario usuario);
    List<Usuario> getUsuarios();
    Optional<Usuario> getUsuarioById(int id);

}
