package com.crud.prueba.common.model.DTOs;

public class UsuarioDto {

    private String nombre;
    private String apellido;
    private String profesion;
    private String email;

    public UsuarioDto(String nombre, String apellido, String profesion, String email) {

        this.nombre = nombre;
        this.apellido = apellido;
        this.profesion = profesion;
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
