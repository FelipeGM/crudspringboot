package com.crud.prueba.web.api.v1.usuario;

import com.crud.prueba.common.model.DTOs.UsuarioDto;
import org.springframework.http.ResponseEntity;

public interface IUsuarioApi {
    ResponseEntity create(UsuarioDto usuarioDto);
    ResponseEntity findAll();
    ResponseEntity findById(int id);
}
