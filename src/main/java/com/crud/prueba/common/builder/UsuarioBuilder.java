package com.crud.prueba.common.builder;

import com.crud.prueba.common.model.entity.Usuario;

public class UsuarioBuilder {

    private int id;
    private String nombre;
    private String apellido;
    private String profesion;
    private String email;

    public UsuarioBuilder() {
    }

    public UsuarioBuilder setId(int id) {
        this.id = id;
        return  this;
    }

    public UsuarioBuilder setNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public UsuarioBuilder setApellido(String apellido) {
        this.apellido = apellido;
        return this;
    }

    public UsuarioBuilder setProfesion(String profesion) {
        this.profesion = profesion;
        return this;
    }

    public UsuarioBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public Usuario build(){
        return new Usuario(id,nombre,apellido,profesion,email);
    }
}
