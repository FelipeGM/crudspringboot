package com.crud.prueba.service.usuario;

import com.crud.prueba.common.model.DTOs.UsuarioDto;
import org.springframework.http.ResponseEntity;

public interface IUsuarioService {

    ResponseEntity create(UsuarioDto usuarioDto);
    ResponseEntity findAll();
    ResponseEntity findById(int id);
}
