package com.crud.prueba.service.usuario;

import com.crud.prueba.common.builder.UsuarioDtoBuilder;
import com.crud.prueba.common.converter.UsuarioConverter;
import com.crud.prueba.common.model.DTOs.UsuarioDto;
import com.crud.prueba.common.model.entity.Usuario;
import com.crud.prueba.repository.usuario.impl.IUsuarioRepositoryFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

    @Autowired
    private IUsuarioRepositoryFacade iUsuarioRepositoryFacade;

    @Autowired
    private UsuarioConverter usuarioConverter;

    @Override
    public ResponseEntity create(UsuarioDto usuarioDto) {
        Optional<Usuario> response =
                iUsuarioRepositoryFacade.create(usuarioConverter.usuarioDtoToUsuario(usuarioDto));
        if (response.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Error: No se pudo crear el usuario");
        }
    }

    @Override
    public ResponseEntity findAll() {

        List<UsuarioDto> listaRespuesta =
                iUsuarioRepositoryFacade.getUsuarios()
                        .stream()
                        .map(x -> new UsuarioDtoBuilder()
                                .setNombre(x.getNombre())
                                .setApellido(x.getApellido())
                                .setProfesion(x.getProfesion())
                                .setEmail(x.getEmail())
                                .build())
                        .collect(Collectors.toList());

        return ResponseEntity.ok(listaRespuesta);
    }

    @Override
    public ResponseEntity findById(int id) {
        Optional<Usuario> response = iUsuarioRepositoryFacade.getUsuarioById(id);
        if (response.isPresent()) {
            return ResponseEntity.ok(usuarioConverter.UsuarioToUsuarioDto(response.get()));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
